# -*- encoding: iso8859-1 -*-

import logging
from datetime import date
from urllib import urlopen

from templates import *

logging.basicConfig()
logger = logging.getLogger("scraper")
logger.setLevel(logging.INFO)

concat = lambda x: "\n".join(x)


def parse_date(datestr):
   "from finnish str rep to date obj"
   datestr.strip()
   d, m, y = datestr.split(".")
   return date(int(y),int(m),int(d))


def parse_cached(datadir, govts_fname, cab_fname):
   try:
      with open(datadir + os.sep + govts_fname) as gfile:
         govts_html = gfile.read()
   except IOError, e:
      logger.error(u"datahakemisto ei sisällä hallituksia luetteloivaa html-sivua")
      sys.exit("ohjelman suoritus keskeytetään.")  

   govts = parse_governments(govts_html)
   logger.info("hallitusluettelon luku onnistui")

   logger.info("luetaan hallitusten kokoonpanot ... (hetkinen)")  
   cabs = {}
   for g in govts:
      try:
         with open(datadir + os.sep + cab_fname % g[0]) as cab_file:
            cab_html = cab_file.read()
            cabs[g[0]] = parse_cabinet(cab_html)
      except IOError, e:
         logger.error(u"datahakemisto ei sisällä hallituksen %i kokoonpanoa sisältävää html-sivua" % g[0])
         sys.exit("ohjelman suoritus keskeytetään.")
      
   logger.info("hallitusten kokoonpanojen luku onnistui")
   return (govts, cabs)


def output_xml(govts, cabs):

   govts_out = []
   for g in govts:
      cab = cabs[g[0]]

      psts_out = []
      for pos in cab:

         msts_out = []
         for mst in cab[pos]:
            msts_out.append(tmpl_minister.strip() % mst)
         txt = concat(msts_out)

         psts_out.append(tmpl_position % (pos, txt))

      txt = concat(psts_out)

      g_begin = g[-5].strftime("%d.%m.%Y")
      g_end = "" if not g[-4] else g[-4].strftime("%d.%m.%Y")
      govts_out.append(tmpl_government % (g[0],g[-2],g[-3], g[1], g_begin, g_end, txt))

   txt = concat(govts_out)
   return tmpl_main.strip() % txt
