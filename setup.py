from setuptools import setup, find_packages
import sys, os

version = '0.9.1'

setup(name='valtioneuvosto_scraper',
      version=version,
      description="scraper to parse data about Finnish governments at www.valtioneuvosto.fi",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='Petri Savolainen',
      author_email='petri.savolainen@koodaamo.fi',
      url='https://bitbucket.org/koodaamo/valtioneuvosto_scraper',
      license='AGPLv3',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      scripts = ["scripts/vn_scraper"],
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
